from typing import NewType


EntityId = NewType('EntityId', str)